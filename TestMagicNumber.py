import numpy as np
import pandas as pd
import treetaggerwrapper as tt
from nltk.corpus import stopwords
from nltk.stem.porter import * 
from gensim.models.word2vec import Word2Vec
from sklearn.tree import DecisionTreeClassifier
from PyQt5.QtWidgets import *
from PyQt5.QtGui import * 
from PyQt5.QtCore import Qt
import pickle
import tweepy
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import sys


##################################################################################


#procitaj podatke
def readDataset(path):

    f = open(path, "r", encoding='utf-8')
    lines = f.readlines()

    sentiments = [] #lista sentimenata
    tweets = [] #lista tweetova

    for l in range(0, len(lines)):
        lparts = lines[l].split(",")
        if(len(lparts) == 2 and len(lparts[1]) != 0):
            sentiments.append(lparts[0])
            tweets.append(lparts[1])

    return [sentiments, tweets]


##################################################################################


#vrati prosjecne vektore recenica
def getAvgVec(model, proTweets, filename = ""):

    vecs = [] #lista svih prosjecnih vektora

    for proT in proTweets:
            
        addVec = 0 #zbroj vektora rijeci u recenici
        nonExist = 0 #broj rijeci koje ne postoje u rijecniku
            
        for word in proT:
            if word in model.wv:
                addVec += model.wv[word]
            else:
                nonExist += 1
        
        if(len(proT) - nonExist != 0):
            avgVec = addVec/(len(proT) - nonExist) #prosjecni vektor recenice
        else:
            avgVec = [0]*100

        vecs.append(avgVec)

        if(filename != ""):
            f = open(filename, "a") #dokument za spremanje vektora
            for a in range(len(avgVec)): #zapisi u file
                f.write(str(avgVec[a]))
                if(a != len(avgVec)-1):
                    f.write(",")
            f.write("\n")

    return vecs


#izracunaj vektore za testne tekstove
def getTestVecs(model, rawTweets):
    if(isinstance(rawTweets, str)):
        vecs = getAvgVec(model, [rawTweets])
        return vecs[0]
    elif(isinstance(rawTweets, list)):
        vecs = getAvgVec(model, rawTweets)
        return vecs


##################################################################################


#analiziraj sentimente i zapisi ih u dokument
def getResults(startpath, endpath):

    lines = readDataset(startpath) #linije dokumenta
    #1. Odrezi interval linija
    sentiments = lines[0]
    tweets = lines[1]
    ef = open(endpath, "a", encoding='utf-8') #dokument za rezultate

    #2. Razdvoji tweetove na rijeci
    splitTweets = []
    for t in tweets:
        t = t[0:len(t)-1].split(" ")
        splitTweets.append(t)

    #3. Ucitaj model i stablo odluke
    model = Word2Vec.load("MyModel")
    treeFile = open("MyTree.pkl", 'rb')
    tree = pickle.load(treeFile)
    treeFile.close()

    #4. Izracunaj vektore za testne tekstove
    test = getTestVecs(model, splitTweets)

    #5. Predvidi rezultate
    predictions = tree.predict(test)

    #6. Pronadi postotke za sentimente i preciznost
    
    #brojaci za: positive-word2, neutral-word2, negative-word2, 
    #positive-vader, neutral-vader, negatve-vader, accuracy
    results = [0, 0, 0, 0, 0, 0, 0] 

    for p in range(len(predictions)):

        if(predictions[p] == 'positive'):
            results[0] += 1
        if(predictions[p] == 'neutral'):
            results[1] += 1
        if(predictions[p] == 'negative'):
            results[2] += 1
            
        if(sentiments[p] == 'positive'):
            results[3] += 1
        if(sentiments[p] == 'neutral'):
            results[4] += 1
        if(sentiments[p] == 'negative'):
            results[5] += 1

        if(predictions[p] == sentiments[p]):
            results[6] += 1

    #7. Zapisi u dokument
    ef.write(getNegNeuPos(startpath) + "\n")
    ef.write("\nPredictions\n")
    ef.write("positive," + str(results[0]/len(predictions)) + "\n")
    ef.write("neutral," + str(results[1]/len(predictions)) + "\n")
    ef.write("negative," + str(results[2]/len(predictions)) + "\n")
    ef.write("\nVader\n")
    ef.write("positive," + str(results[3]/len(predictions)) + "\n")
    ef.write("neutral," + str(results[4]/len(predictions)) + "\n")
    ef.write("negative," + str(results[5]/len(predictions)) + "\n")
    ef.write("\naccuracy," + str(results[6]/len(predictions)) + "\n")

    print(str(results[6]/len(predictions)))


##################################################################################


#provjeri je li sentiment isti kao i prije
def checkSentiment(line, magicNumber):
    
    lineSeparate = line.split(",")
    sentiment = lineSeparate[0]
    tweet = lineSeparate[1]

    sentimentNew = vaderAnalizeNumber(tweet, magicNumber)

    if(sentimentNew == sentiment):
        return True
    else:
        return False


#napravi novi dataset za treniranje
def createOgDataset(startpath, endpath):

    lines = open(startpath, 'r').readlines() #linije dokumenta
    ef = open(endpath, "a", encoding='utf-8') #dokument za zapisivanje
    
    for l in lines:
        if(checkSentiment(l, 0.1)):
            ef.write(l)


##################################################################################


#dohvati omjer negativnog, neutralnog i pozitivnog sentimenta
def getNegNeuPos(startpath):

    lines = open(startpath, 'r').readlines() #linije dokumenta
    neg = 0
    neu = 0
    pos = 0

    for l in lines:
        senti = l.split(",")[0]
        if(senti == "negative"):
            neg += 1
        elif(senti == "neutral"):
            neu += 1
        else:
            pos += 1    

    return 'neg: ' + str(neg) + ', neu: ' + str(neu) + ', pos: ' + str(pos)


#odredi sentiment pomocu Vadera za broj
def vaderAnalizeNumber(tweet, magicNumber):

    vaderAnalyzator = SentimentIntensityAnalyzer() #analizator sentimenata
    sentimentDict = vaderAnalyzator.polarity_scores(tweet)
    
    #vrati ispravnu oznaku
    if(sentimentDict["neg"] > magicNumber or sentimentDict["pos"] > magicNumber):
        if(sentimentDict["neg"] > sentimentDict["pos"]):
            return "negative"
        elif(sentimentDict["neg"] < sentimentDict["pos"]):
            return "positive"
        else:
            return "neutral"
    else:
        return "neutral"


##################################################################################


#napravi novi dataset s novim brojem
def createDatasetNumber(startpath, endpath, magicNumber):
    
    lines = open(startpath, 'r').readlines() #linije dokumenta
    ef = open(endpath, "a", encoding='utf-8') #dokument za zapisivanje
    
    for l in lines:
        tweet = l.split(",")[1]
        sentiment = vaderAnalizeNumber(tweet, magicNumber)
        ef.write(sentiment + "," + tweet)



##################################################################################


#GLAVNA FUNKCIJA

def main():

    #1. Kreiraj skupove (train i test) podataka za neki broj
    magicNumber = 0.8
    path = "c://Users//Sugarplum//Desktop//luciFER//Seminar1//TestingTheMagicNumber//Datasets//"
    
    startTrain = path + "train//0.1.txt"
    endTrain = path + "train//" + str(magicNumber) + ".txt"

    startTest = path + "test//0.1.txt"
    endTest = path + "test//" + str(magicNumber) + ".txt"
    
    createDatasetNumber(startTrain, endTrain, magicNumber)
    createDatasetNumber(startTest, endTest, magicNumber)

    #2. Provjeri omjere sentimenata
    print(getNegNeuPos(endTrain))
    print(getNegNeuPos(endTest))

    #3. Procitaj podatke
    parts = readDataset(endTrain)
    sentiments = parts[0]
    tweets = parts[1]

    #4. Treniraj Word2Vec s podatcima i spremi model
    splitTweets = []
    for t in tweets:
        t = t[0:len(t)-1].split(" ")
        splitTweets.append(t)

    model = Word2Vec(splitTweets, vector_size = 100, min_count=1, window=5, workers=3, sg=0)
    model.save("MyModel")

    #5. Izracunaj prosjecne vektore recenica i zapisi u dokument
    vecs = getAvgVec(model, splitTweets, filename = "MyVecs.txt")

    #6. Treniraj stablo odluke sa sentimentima i spremi ga
    tree = DecisionTreeClassifier(max_depth = 10)
    tree.fit(vecs, sentiments)
    tree_file = open("MyTree.pkl", "wb")
    pickle.dump(tree, tree_file)
    tree_file.close()
 
    #7. Testiraj model i premi podatke
    resultTrain = path + "resultsTrain//" + str(magicNumber) + ".txt"
    resultTest = path + "resultsTest//" + str(magicNumber) + ".txt"
    getResults(endTrain, resultTrain)
    getResults(endTest, resultTest)

    
if __name__ == "__main__":
    main()
